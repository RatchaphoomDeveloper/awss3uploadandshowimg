const s3Bucket = require("../config/awsConnection");
const fileType = require('file-type');
const { ACL,S3Url } = process.env;

const imageUpload = async (path, buffer) => {
  const bufferFileType = await fileType.fromBuffer(buffer.data ? buffer.data : buffer)
  const data = {
    Key: path,
    Body:buffer.data ? buffer.data : buffer,
    ContentEncoding: buffer.encoding ?  buffer.encoding : "7bit",
    ContentType: buffer.mimetype ? buffer.mimetype : bufferFileType.mime,
    ACL: ACL,
  };
  return new Promise((resolve, reject) => {
    s3Bucket.putObject(data, (err) => {
      if (err) {
          reject(err)
      } else {
        resolve(S3Url+"/"+ path);
      }
    });
  });
};


module.exports = imageUpload