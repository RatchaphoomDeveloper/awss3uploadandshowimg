const getImageBuffer = require("./getImageBuffer");
const imageUpload = require("./imageUpload");

const getImageUrl = async (type, base64Image) => {
  const buffer = getImageBuffer(base64Image);
  const currentTime = new Date().getTime();
  return imageUpload(`${type}/${currentTime}.jpeg`, buffer);
};

module.exports = getImageUrl;
