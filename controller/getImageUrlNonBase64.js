const getImageBuffer = require("./getImageBuffer");
const imageUpload = require("./imageUpload");

const getImageUrlNonBase64 = async (type, data) => {
  const currentTime = new Date().getTime();
  return imageUpload(`${type}/${currentTime+data.files.name}`, data.files);
//   return "test"
};

module.exports = getImageUrlNonBase64;
