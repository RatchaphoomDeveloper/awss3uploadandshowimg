const AWS = require('aws-sdk');
const {AWS_ACCESS_KEY_ID,AWS_SECRET_ACCESS_KEY,REGION} = process.env

AWS.config.update({
  accessKeyId:AWS_ACCESS_KEY_ID,
  secretAccessKey:AWS_SECRET_ACCESS_KEY,
  region:  REGION
})

const s3Bucket = new AWS.S3({params:{Bucket:'jikosocial'}})


module.exports = s3Bucket


