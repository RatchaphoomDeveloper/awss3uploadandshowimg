var express = require("express");
const s3Bucket = require("../config/awsConnection");
const converttobase64 = require("../controller/convertTobase64");
const getImageUrl = require("../controller/getImageUrl");
const getImageUrlNonBase64 = require("../controller/getImageUrlNonBase64");
var router = express.Router();

const { BUCKET_NAME } = process.env;

/* GET home page. */
router.get("/getBuckets", function (req, res, next) {
  const s3 = s3Bucket.listBuckets(function (err, data) {
    if (err) {
      // console.log("Error", err);
      return res.status(400).json({ response: err.Buckets, message: "Error" });
    } else {
      // console.log("Success", data.Buckets);
      return res
        .status(200)
        .json({ response: data.Buckets, message: "Success" });
    }
  });
});

router.get("/getObjectInBucket", function (req, res, next) {
  var bucketParams = {
    Bucket: BUCKET_NAME,
  };
  s3Bucket.listObjects(bucketParams, function (err, data) {
    if (err) {
      // console.log("Error", err);
      return res.status(400).json({ response: err, message: "Error" });
    } else {
      // console.log("Success", data);
      return res.status(200).json({ response: data, message: "Success" });
    }
  });
});

router.post("/insertImageToBucket", async (req, res, next) => {
  try {
    const { files } = req;
    const {base64} = req.body
    if (files) {
      const uploadWithForm = await getImageUrlNonBase64(BUCKET_NAME, files);
      return res
      .status(200)
      .json({ response: uploadWithForm, message: "Success" });
    } else {
      const uploadWithForm = await getImageUrl(BUCKET_NAME,base64)
      return res
      .status(200)
      .json({ response: uploadWithForm, message: "Success" });
    }
    // const convertBase64 = await converttobase64(files)
    // console.log(convertBase64)
    
  } catch (error) {
    return res.status(400).json({ response: error.message, message: "Error" });
  }
});

module.exports = router;
